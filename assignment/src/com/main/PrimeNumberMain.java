package com.main;
import com.model.PrimeNumber;
import java.util.Scanner;
public class PrimeNumberMain {

	public static void main(String[] args) {
		Scanner scanner=new Scanner(System.in);
		System.out.println("Enter a value:");
		int a=scanner.nextInt();
		PrimeNumber prime=new PrimeNumber();
		int temp=prime.primeNumber(a);
		if(temp==1)
		{
			System.out.println(a+" is a prime number");
		}
		else
		{
			System.out.println(a+" is a not prime number");

	}

	}
}
