package com.model;

public class EmployeeA {
	int employeeId;
	String employeeName;
	int employeeAge;
	public EmployeeA(int employeeId, String employeeName, int employeeAge) {
		super();
		this.employeeId = employeeId;
		this.employeeName = employeeName;
		this.employeeAge = employeeAge;
	}

	public void display()
	{
		System.out.println("Parent class");
	}


}
