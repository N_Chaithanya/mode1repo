package com.main;
import java.util.Scanner;

import com.model.VowelCount;
public class VowelCountMain {

	public static void main(String[] args) {
		Scanner scanner=new Scanner(System.in);
		System.out.println("Enter the string:");
		String str=scanner.next();
		VowelCount vowelcount=new VowelCount();
		System.out.println("The total vowels in a string is:"+vowelcount.vowelsCount(str));
		

	}

}
