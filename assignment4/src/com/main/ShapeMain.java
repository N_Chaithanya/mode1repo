package com.main;
import com.model.Shape;
import com.model.Square;
import com.model.Circle;
import com.model.Rectangle;
public class ShapeMain {
	public static void main(String[] args) {
	
	Circle circle=new Circle("circle",2);
	System.out.println(circle.calculateArea());
	Rectangle rectangle=new Rectangle("rectangle",2,3);
	System.out.println(rectangle.calculateArea());
	Square square=new Square("square",2);
	System.out.println(square.calculateArea());
	}
}
